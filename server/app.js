require(`./config/config`)
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const rtsmail = require(`./routes/mail.routes`)

const port =  process.env.PORT
const app = express()


app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true
  })
)

app.use(cors());



app.get(``, (req,res) => {
    console.log('connecting the database')
   
    res.status(200).json("success in connecting ")
}
)
app.use(`/mail`, rtsmail)

app.listen(port, () => 
    console.log(`App running on port ${port}.`)
  )