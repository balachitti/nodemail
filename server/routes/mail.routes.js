const express = require('express')
const router = express.Router()
const mail = require(`../controller/mail.controller`)

router.post(`/`, mail.create)
router.get(`/`,mail.display)

module.exports = router